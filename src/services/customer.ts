import type Customer from "@/types/Customer";
import http from "./axios"
function getCustomers(params: any) {
  return http.get("/customers", {params: params});
}
function saveCustomers(customer: Customer) {

  return http.post("/customers", customer);
}

function updateCustomers(id:number, cus: Customer) {
  return http.patch(`/customers/${id}`, cus);
}

function delCustomers(id:number) {
  return http.delete(`/customers/${id}`);
}
export default { getCustomers,saveCustomers,updateCustomers, delCustomers};