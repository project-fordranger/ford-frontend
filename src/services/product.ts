import type Product from "@/types/Product";
import http from "./axios"

function getProductsByCategory(category: number) {
  return http.get(`/products/category/${category}`);
}

function getProducts(params: any) {
  return http.get("/products", {params: params});
}

function getProductsByQuery() {
  return http.get("/reports1/products" );
}


function saveProducts(product: Product & {files: File[]}) {
  const formData = new FormData();
  formData.append("name", product.name);
  formData.append("price", `${product.price}`);
  formData.append("file", product.files[0]);
  formData.append("categoryId",`${product.categoryId}`);
  return http.post("/products", formData, {headers: {
    'Content-Type': 'multipart/form-data'
}});
}

function updateProducts(id:number, product: Product & {files: File[]}) {
  const formData = new FormData();
  formData.append("name", product.name);
  formData.append("price", `${product.price}`);
  if(product.files) {
    formData.append("file", product.files[0]);
  }
  return http.patch(`/products/${id}`, formData, {headers: {
    'Content-Type': 'multipart/form-data'
}});
}

function delProducts(id:number) {
  return http.delete(`/products/${id}`);
}

export default {getProducts, saveProducts, updateProducts, delProducts,getProductsByCategory,getProductsByQuery};