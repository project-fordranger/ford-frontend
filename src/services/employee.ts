import type Employee from "@/types/Employee";
import http from "./axios"
function getEmployees() {
  return http.get("/employees");
}
function saveEmployees(employee: Employee & {files: File[]}) {
  const formData = new FormData();
  formData.append("name", employee.name);
  formData.append("address", employee.address);
  formData.append("tel", employee.tel);
  formData.append("email", employee.email);
  formData.append("position", employee.position);
  formData.append("timetowork", employee.timetowork);
  formData.append("timeoff", employee.timeoff);
  formData.append("hourly", `${employee.hourly}`);
  formData.append("salary", `${employee.salary}`);
  formData.append("file", employee.files[0]);
  return http.post("/employees", formData , {headers: {
    'Content-Type': 'multipart/form-data'
}});
}
function updateEmployees(id:number, employee: Employee & {files: File[]}) {
  const formData = new FormData();
  formData.append("name", employee.name);
  formData.append("address", employee.address);
  formData.append("tel", employee.tel);
  formData.append("email", employee.email);
  formData.append("position", employee.position);
  formData.append("timetowork", employee.timetowork);
  formData.append("timeoff", employee.timeoff);
  formData.append("hourly", `${employee.hourly}`);
  formData.append("salary", `${employee.salary}`);
  if(employee.files) {
    formData.append("file", employee.files[0]);
  }
  return http.patch(`/employees/${id}`, formData , {headers: {
    'Content-Type': 'multipart/form-data'
}});
}

function delEmployees(id:number) {
  return http.delete(`/employees/${id}`);
}
export default { getEmployees,saveEmployees,updateEmployees, delEmployees};