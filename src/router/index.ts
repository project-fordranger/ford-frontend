import LoginView from "@/views/LoginView.vue";
import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "homepage",
      components: {
        default: () => HomeView,
        header: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/login",
      name: "login",

      components: {
        default: LoginView,
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/pointofsell",
      name: "pointheader",

      components: {
        default: () => import("../views/PointOfSell.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/posl",
      name: "posl",

      components: {
        default: () => import("../views/POSsumrong.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "MainLayout",
      },
    },

    {
      path: "/prodlist",
      name: "prodlist",

      components: {
        default: () => import("../views/products/ProductList.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/userlist",
      name: "userlist",

      components: {
        default: () => import("../views/users/UserList.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/cuslist",
      name: "cuslist",

      components: {
        default: () => import("../views/customers/CusList.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/matlist",
      name: "matlist",

      components: {
        default: () => import("../views/material/MaterialList.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/emplist",
      name: "emplist",

      components: {
        default: () => import("../views/Employee/EmployeeList.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/orderlist",
      name: "orderlist",

      components: {
        default: () => import("../views/order/OrderList.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/orderlistrep",
      name: "orderlistrep",

      components: {
        default: () => import("../views/order/OrderListRep.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/prolistquery",
      name: "prolistquery",

      components: {
        default: () => import("../views/products/ProductListQuery.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/dashboard",
      name: "dashboard",

      components: {
        default: () => import("../views/DashBoard.vue"),
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "MainLayout",
        requiresAuth: true,
      },
    },

  ],
});
function isLogin(){
  const user = localStorage.getItem("user")
    if(user) {
      return true;
    }
    return false;
}
router.beforeEach((to, from) => {
  // instead of having to check every route record with
  // to.matched.some(record => record.meta.requiresAuth)
  if (to.meta.requiresAuth && !isLogin()) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    return {
      path: '/login',
      // save the location we were at to come back later
      query: { redirect: to.fullPath },
    }
  }
})
export default router;
