import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useSuccessStore = defineStore("success", () => {
  const snackbar = ref(false);
  const success = ref("");

  function showsuccess(text: string) {
    success.value = text;
    snackbar.value = true;
  }
  return { snackbar, success, showsuccess };
});
