import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type Qrcode from '@/types/Qrcode';
import qrcodeService from "@/services/qrcode"
import { useLoadingStore } from './loading';
import { useMessageStore } from './message';
import { useSuccessStore } from './success';

export const useQrcodeStore = defineStore('qrcode', () => {
    const loadingStore = useLoadingStore();
    const messageStore = useMessageStore();
    const successStore = useSuccessStore();
    const dialog = ref(false);
    // const page = ref(1);
    // const take = ref(2);
    const qrcodes = ref<Qrcode[]>([]);
    const editedQrcode = ref<Qrcode>({ qrcodeamount: 0 });
  
  
   
  
    watch(dialog, (newDialog, oldDialog) => {
      if (!newDialog) {
        editedQrcode.value= { qrcodeamount: 0 };
      }
    });
    async function getQrcodes() {
      loadingStore.isLoading = true
    try {
      const res = await qrcodeService.getQrcodes({});
      qrcodes.value = res.data;
      console.log(res);
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูลได้")
    }
    loadingStore.isLoading = false
    }
    async function saveQrcode() {
      loadingStore.isLoading = true
      try {
        const res = await qrcodeService.saveQrcodes(editedQrcode.value);
        dialog.value = false;
        await getQrcodes();
        successStore.showsuccess("ชำระเงินสำเร็จ")
      } catch(e) {
        console.log(e);
        messageStore.showError("ไม่สามารถชำระเงินได้")
      }
      loadingStore.isLoading = false
    }
    // async function deleteCustomer(id: number) {
    //   loadingStore.isLoading = true
    //   try {
    //     const res = await customerService.delCustomers(id);
    //     await getCustomers();
    //     successStore.showsuccess("ลบสำเร็จ")
    //   } catch(e) {
    //     console.log(e);
    //     messageStore.showError("ไม่สามารถลบข้อมูล Customer ได้")
    //   }
    //   loadingStore.isLoading = false
    // }
  
    function editQrcode(qrcode: Qrcode) {
      editedQrcode.value = JSON.parse(JSON.stringify(qrcode));;
      dialog.value = true;
    }
  
    return {
      dialog,
      qrcodes,
      getQrcodes,
      editedQrcode,
      saveQrcode,
      editQrcode,
    };
  });
