import type User from "@/types/User";
import http from "./axios"
function getUsers() {
  return http.get("/users");
}

function saveUsers(user: User & {files: File[]}) {
  const formData = new FormData();
  formData.append("name", user.name);
  formData.append("email", user.email);
  formData.append("password", user.password);
  formData.append("file", user.files[0]);
  return http.post("/users", formData , {headers: {
    'Content-Type': 'multipart/form-data'
}});
}
function updateUsers(id:number, user: User & {files: File[]}) {
  const formData = new FormData();
  formData.append("name", user.name);
  formData.append("email", user.email);
  formData.append("password", user.password);
  if(user.files) {
    formData.append("file", user.files[0]);
  }
  return http.patch(`/users/${id}`, formData , {headers: {
    'Content-Type': 'multipart/form-data'
}});
}

function delUsers(id:number) {
  return http.delete(`/users/${id}`);
}
export default {getUsers, saveUsers,  updateUsers, delUsers};