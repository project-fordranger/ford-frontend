export default interface Cash{
    id?: number;
    cashsum: number;
    cashamount: number;
    cashchange: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}