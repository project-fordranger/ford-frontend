import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import materialService from "@/services/material"
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Material from "@/types/Material";
import { useSuccessStore } from "./success";
export const useMaterialStore = defineStore("material", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const successStore = useSuccessStore();
  const dialog = ref(false);
  const materials = ref<Material[]>([]);
  const editedMaterial = ref<Material & {files: File[] }>({ name: "",  unit: 0,stockIn:"", expire: "",  amountLeft: 0 ,minimun: 0,image:"No-Image-Placeholder.jpg",files: []});


  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedMaterial.value= {  name: "",  unit: 0,stockIn:"", expire: "",amountLeft: 0 ,minimun: 0,image:"No-Image-Placeholder.jpg",files: []};
    }
  });
  async function getMaterials() {
    loadingStore.isLoading = true
  try {
    const res = await materialService.getMaterials();
    materials.value = res.data;
    console.log(res);
  } catch(e) {
    console.log(e);
    messageStore.showError("ไม่สามารถเรียกข้อมูล Material ได้")
  }
  loadingStore.isLoading = false
  }
  async function saveMaterial() {
    loadingStore.isLoading = true
    try {
      if(editedMaterial.value.id) {
        const res = await materialService.updateMaterials(editedMaterial.value.id,editedMaterial.value);
      } else {
        const res = await materialService.saveMaterials(editedMaterial.value);
      }
      dialog.value = false;
      await getMaterials();
      successStore.showsuccess("บันทึกสำเร็จ")
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Material ได้")
    }
    loadingStore.isLoading = false
  }
  async function deleteMaterial(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await materialService.delMaterials(id);
      await getMaterials();
      successStore.showsuccess("ลบสำเร็จ")
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Material ได้")
    }
    loadingStore.isLoading = false
  }

  function editMaterial(material: Material) {
    editedMaterial.value = JSON.parse(JSON.stringify(material));;
    dialog.value = true; 
  }

  return {
    dialog,
    materials,
    getMaterials,
    editedMaterial,
    saveMaterial,
    deleteMaterial,
    editMaterial

  };
});
