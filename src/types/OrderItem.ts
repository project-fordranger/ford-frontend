import type Order from "./Order";
import type Product from "./Product";

export default interface OrderItem{
  id?: number;
  name?: string;
  price?: number;
  amount?: number;
  total?: number;
  order?: Order;
  product?: Product; // Product Id
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}