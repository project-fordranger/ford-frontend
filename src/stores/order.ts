import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Order from "@/types/Order";
import orderService from "@/services/order"
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { useSuccessStore } from "./success";
import type Product from "@/types/Product";
import product from "@/services/product";
import { useAuthStore } from "./auth";
import PointOfSellVue from "@/views/PointOfSell.vue";
import type Customer from "@/types/Customer";
export const useOrderStore = defineStore("order", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authStore = useAuthStore();
  const successStore = useSuccessStore();
  const dialog = ref(false);
  const dialogrep = ref(false);
  const orders = ref<Order[]>([]);
  const editedOrder = ref<Order>({discount: 0});
  
  const orderItems = ref<{ name: string; amount: number; price: number; total: number}[]>([]);
  const orderList = ref<{ product: Product; amount: number; sum: number; discount: number;}[]>([]);


  function clearOrder() {
    orderList.value = [];
  }

  function addProduct(item: Product) {
    for (let i = 0; i < orderList.value.length; i++) {
      if (orderList.value[i].product.id === item.id) {
        orderList.value[i].amount++;
        orderList.value[i].sum = orderList.value[i].amount * item.price;
        return;
      }
    }
    orderList.value.push({ product: item, amount: 1, sum: 1 * item.price, discount: 0 });
  }
  function deleteProduct(index: number) {
    orderList.value.splice(index, 1);
  }
  const sumAmount = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].amount;
    }
    return sum;
  })
  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].sum;
    }
    return sum;
  })

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedOrder.value = {};
    }
  });

  async function getOrders() {
    loadingStore.isLoading = true
    try {
      const res = await orderService.getOrders();
      orders.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถเรียกข้อมูล Order ได้")
    }
    loadingStore.isLoading = false
  }
  async function openOrder() {
    loadingStore.isLoading = true;
    const user: {id: number } = authStore.getUser();
    const orderItems = orderList.value.map((item) =>
      <{ productId: number, amount: number }>{
        productId: item.product.id,
        amount: item.amount
      }
    );
    
    const order = {userId: user.id , orderItems: orderItems };
    console.log(order)
    
    try {
      const res = await orderService.saveOrders(order);
      dialog.value = false;
      successStore.showsuccess("ทำรายการเสร็จสิ้น")
      // location.reload()
      await getOrders();
      clearOrder();
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Order ได้")
    }
    loadingStore.isLoading = false
  }

  async function delOrderProcedures() {
    loadingStore.isLoading = true
  try {
    const res = await orderService.delOrderProcedures();
    orders.value = res.data;
    console.log(res);
  } catch(e) {
    console.log(e);
    messageStore.showError("ไม่สามารถลบได้")
  }
  loadingStore.isLoading = false
  }

  async function getOrderCharts() {
    loadingStore.isLoading = true
  try {
    const res = await orderService.getOrderCharts();
    orders.value = res.data[0];
    console.log(res);
  } catch(e) {
    console.log(e);
    messageStore.showError("ไม่สามารถลบได้")
  }
  loadingStore.isLoading = false
  }
  // async function deleteOrder(id: number) {
  //   loadingStore.isLoading = true
  //   try {
  //     const res = await orderService.delOrders(id);
  //     await getOrders();
  //     successStore.showsuccess("ลบสำเร็จ")
  //   } catch (e) {
  //     console.log(e);
  //     messageStore.showError("ไม่สามารถลบข้อมูล Order ได้")
  //   }
  //   loadingStore.isLoading = false
  // }

  

  function editOrder(order: Order) {
    editedOrder.value = JSON.parse(JSON.stringify(order));;
    dialog.value = true;
  }

  return {
    dialog,
    orders,
    getOrders,
    editedOrder,
    openOrder,
    // deleteOrder,
    editOrder,
    addProduct,
    deleteProduct,
    sumAmount,
    sumPrice,
    orderList,
    delOrderProcedures,
    dialogrep,
    getOrderCharts
  };
});
