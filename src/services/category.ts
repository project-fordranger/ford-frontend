import type Category from "@/types/Category";
import http from "./axios"
function getCategories() {
  return http.get("/Categories");
}

function saveCategory(Category: Category) {
  return http.post("/Categories", Category );
}
function updateCategory(id:number, Category: Category) {
  return http.patch(`/Categories/${id}`, Category);
}

function delCategory(id:number) {
  return http.delete(`/Categories/${id}`);
}
export default {getCategories, saveCategory,  updateCategory, delCategory};