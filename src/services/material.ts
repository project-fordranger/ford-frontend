import type Material from "@/types/Material";
import http from "./axios"
function getMaterials() {
  return http.get("/materials");
}
function saveMaterials(material: Material & {files: File[]}) {
  const formData = new FormData();
  formData.append("name", material.name);
  formData.append("unit", `${material.unit}`);
  formData.append("stockIn", material.stockIn);
  formData.append("expire", material.expire);
  formData.append("amountLeft", `${material.amountLeft}`);
  formData.append("minimun", `${material.minimun}`);
  formData.append("file", material.files[0]);
  return http.post("/materials", formData ,{headers: {
    'Content-Type': 'multipart/form-data'
}});
}
function updateMaterials(id:number, material: Material & {files: File[]}) {
  const formData = new FormData();
  formData.append("name", material.name);
  formData.append("unit", `${material.unit}`);
  formData.append("stockIn", material.stockIn);
  formData.append("expire", material.expire);
  formData.append("amountLeft", `${material.amountLeft}`);
  formData.append("minimun", `${material.minimun}`);
  if(material.files) {
    formData.append("file", material.files[0]);
  }
  return http.patch(`/materials/${id}`, formData,{headers: {
    'Content-Type': 'multipart/form-data'}});
}

function delMaterials(id:number) {
  return http.delete(`/materials/${id}`);
}
export default { getMaterials,saveMaterials,updateMaterials, delMaterials};