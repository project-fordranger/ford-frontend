export default interface Cash {
    id?: number;
    qrcodeamount: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}