export default interface Material{
  id?: number;
  name: string;
  unit: number;
  stockIn: string;
  expire: string;
  amountLeft: number;
  minimun: number;
  image?: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}