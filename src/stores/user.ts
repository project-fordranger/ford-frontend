import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import userService from "@/services/user"
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { useSuccessStore } from "./success";
export const useUserStore = defineStore("User", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const successStore = useSuccessStore();
  const dialog = ref(false);
  const users = ref<User[]>([]);
  const editedUser = ref<User & {files: File[] }>({ name: "",  email:"",password: "" ,image:"No-Image-Placeholder.jpg",files: []});
  const login = (loginName: string,password: string): boolean => {
    const index = users.value.findIndex((item) => item.name === loginName);
    if(index >= 0) {
      const user = users.value[index];
      if(user.password === password) {
        return true;
      }
      return false;
    }
    return false;
  }
  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedUser.value= { name: "",  email:"",password: "",image:"No-Image-Placeholder.jpg",files: []};
    }
  });
  async function getUsers() {
    loadingStore.isLoading = true
  try {
    const res = await userService.getUsers();
    users.value = res.data;
    console.log(res);
  } catch(e) {
    console.log(e);
    messageStore.showError("ไม่สามารถเรียกข้อมูล User ได้")
  }
  loadingStore.isLoading = false
  }
  async function saveUser() {
    loadingStore.isLoading = true
    try {
      if(editedUser.value.id) {
        const res = await userService.updateUsers(editedUser.value.id,editedUser.value);
      } else {
        const res = await userService.saveUsers(editedUser.value);
      }
      
      dialog.value = false;
      await getUsers();
      successStore.showsuccess("บันทึกสำเร็จ")
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล User ได้")
    }
    loadingStore.isLoading = false
  }
  async function deleteUser(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await userService.delUsers(id);
      await getUsers();
      successStore.showsuccess("ลบสำเร็จ")
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล User ได้")
    }
    loadingStore.isLoading = false
  }

  function editUser(user: User) {
    editedUser.value = JSON.parse(JSON.stringify(user));;
    dialog.value = true;
  }


  return {
    dialog,
    users,
    getUsers,
    editedUser,
    saveUser,
    deleteUser,
    editUser,
    login

  };
});
