import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import http from "@/services/axios";
import productService from "@/services/product"
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { useSuccessStore } from "./success";

export const useProductStore = defineStore("Product", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const successStore = useSuccessStore();
  const dialog = ref(false);
  const products = ref<Product[]>([]);
  const category = ref(1);
  const reports1 = ref('');
  const reports = ref('');
  const keyword = ref('')
  const editedProduct = ref<Product & {files: File[] }>({ name: "",  price: 0,categoryId: 1,image:"No-Image-Placeholder.jpg",files: []});

  watch(keyword, async (newPage, oldPage) => {
    await getProducts();
  })

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedProduct.value= { name: "",  price: 0 ,categoryId:1 ,image:"No-Image-Placeholder.jpg",files: []};
    }
  });
  watch(category, async (newCategory, oldCategory) => {
    await getProductsByCategory(newCategory);
  })

  watch(reports1, async (newReport, oldReport) => {
    await getProductsByQuery();
  })


  async function getProductsByCategory(category: number) {
    loadingStore.isLoading = true
  try {
    console.log(category);
    const res = await productService.getProductsByCategory(category);
    products.value = res.data;
  } catch(e) {
    console.log(e);
    messageStore.showError("ไม่สามารถเรียกข้อมูล Product ได้")
  }
  loadingStore.isLoading = false
  }

  async function getProducts() {
    loadingStore.isLoading = true
  try {
    const res = await productService.getProducts({keyword: keyword.value});
    products.value = res.data.data;
  } catch(e) {
    console.log(e);
    messageStore.showError("ไม่สามารถเรียกข้อมูล Product ได้")
  }
  loadingStore.isLoading = false
  }

  async function getProductsByQuery() {
    loadingStore.isLoading = true
  try {
    const res = await productService.getProductsByQuery();
    products.value = res.data;
    console.log(res)
  } catch(e) {
    console.log(e);
    messageStore.showError("ไม่สามารถเรียกข้อมูล Product ได้")
  }
  loadingStore.isLoading = false
  }

  async function saveProduct() {
    loadingStore.isLoading = true
    try {
      if(editedProduct.value.id) {
        const res = await productService.updateProducts(editedProduct.value.id,editedProduct.value);
      } else {
        const res = await productService.saveProducts(editedProduct.value);
      }

      dialog.value = false;
      await getProducts();
      successStore.showsuccess("บันทึกสำเร็จ")
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Product ได้")
    }
    loadingStore.isLoading = false
  }
  async function deleteProduct(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await productService.delProducts(id);
      await getProducts();
      successStore.showsuccess("ลบสำเร็จ")
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Product ได้")
    }
    loadingStore.isLoading = false
  }

    function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }
  return {
    dialog,
    products,
    getProducts,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
    category,
    getProductsByCategory,
    keyword,
    getProductsByQuery,
  };
});
