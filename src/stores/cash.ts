import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import cashService from "@/services/cash"
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import { useSuccessStore } from "./success";
import type Cash from "@/types/Cashtype";
export const useCashStore = defineStore("cash", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const successStore = useSuccessStore();
  const dialog = ref(false);
  const cashs = ref<Cash[]>([]);
  const editedCash = ref<Cash>({ cashsum: 0, cashamount: 0 , cashchange: 0});

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedCash.value= { cashsum: 0 ,cashamount: 0 ,cashchange: 0};
    }
  });

  async function getCashs() {
    loadingStore.isLoading = true
  try {
    const res = await cashService.getCashs();
    cashs.value = res.data;
    console.log(res);
  } catch(e) {
    console.log(e);
    messageStore.showError("ไม่สามารถเรียกข้อมูล Cash ได้")
  }
  loadingStore.isLoading = false
  }

  async function saveCash() {
    loadingStore.isLoading = true
    try {
      const res = await cashService.saveCashs(editedCash.value);
      dialog.value = false;
      await getCashs();
      successStore.showsuccess("ชำระเงินสำเร็จ")
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Cash ได้")
    }
    loadingStore.isLoading = false
  }
  
  async function deleteCash(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await cashService.delCashs(id);
      await getCashs();
      successStore.showsuccess("ลบสำเร็จ")
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Cash ได้")
    }
    loadingStore.isLoading = false
  }

  function editCash(cash: Cash) {
    editedCash.value = JSON.parse(JSON.stringify(cash));;
    dialog.value = true;
  }

  return {
    dialog,
    cashs,
    getCashs,
    editedCash,
    saveCash,
    deleteCash,
    editCash,
  };
});
