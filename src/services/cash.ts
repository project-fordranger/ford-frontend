import type Cash from "@/types/Cashtype";
import http from "./axios"
function getCashs() {
  return http.get("/cashs");
}
function saveCashs(cash: Cash) {
  return http.post("/cashs", cash);
}
function updateCashs(id:number, cash: Cash) {
  return http.patch(`/cashs/${id}`, cash);
}

function delCashs(id:number) {
  return http.delete(`/cashs/${id}`);
}
export default { getCashs,saveCashs,updateCashs, delCashs};