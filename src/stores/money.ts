import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type Customer from '@/types/Customer';
import { useLoadingStore } from './loading';
import { useMessageStore } from './message';
import customerService from "@/services/customer"
import { useSuccessStore } from './success';

export const useMoneyStore = defineStore('money', () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const successStore = useSuccessStore();
  const dialog = ref(false);
  // const page = ref(1);
  // const take = ref(2);
  const customers = ref<Customer[]>([]);
  const editedCustomer = ref<Customer>({ name: "",  age: 0,tel: "",gender: "",points: 0,});

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedCustomer.value= { name: "",  age: 0,tel: "",gender: "",points: 0,};
    }
  });
  async function getCustomers() {
    loadingStore.isLoading = true
  try {
    const res = await customerService.getCustomers({});
    customers.value = res.data;
    console.log(res);
  } catch(e) {
    console.log(e);
    messageStore.showError("ไม่สามารถเรียกข้อมูล Customer ได้")
  }
  loadingStore.isLoading = false
  }
  async function saveCustomer() {
    loadingStore.isLoading = true
    try {
      const res = await customerService.saveCustomers(editedCustomer.value);
      dialog.value = false;
      await getCustomers();
      successStore.showsuccess("บันทึกสำเร็จ")
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Customer ได้")
    }
    loadingStore.isLoading = false
  }
  async function deleteCustomer(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await customerService.delCustomers(id);
      await getCustomers();
      successStore.showsuccess("ลบสำเร็จ")
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Customer ได้")
    }
    loadingStore.isLoading = false
  }

  function editCustomer(customer: Customer) {
    editedCustomer.value = JSON.parse(JSON.stringify(customer));;
    dialog.value = true;
  }

  return {
    dialog,
    customers,
    getCustomers,
    editedCustomer,
    saveCustomer,
    deleteCustomer,
    editCustomer,
  };
});
