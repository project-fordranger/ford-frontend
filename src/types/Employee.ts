export default interface Employee{
  id?: number;
  name: string;
  address: string;
  tel: string;
  email: string;
  position: string;
  hourly: number;
  image?: string;
  timetowork: string;
  timeoff: string;
  salary: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}