import type Order from "@/types/Order";
import http from "./axios"
function getOrders() {
  return http.get("/orders" );
}
function saveOrders(order: {
  orderItems: { productId: number, amount: number}[];
  userId: number;
}) {
  return http.post("/orders", order);
}
function updateOrders(id:number, order: Order) {
  return http.patch(`/orders/${id}`, order);
}

function delOrderProcedures() {
  return http.get("/reports1/delorder" );
}

function getOrderCharts() {
  return http.get("/reports1/selldw" );
}

function delOrders(id:number) {
  return http.delete(`/orders/${id}`);
}
export default { getOrders,saveOrders,updateOrders, delOrders,delOrderProcedures, getOrderCharts};