export default interface Product{
  id?: number;
  name: string;
  price: number;
  image?: string;
  categoryId: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}