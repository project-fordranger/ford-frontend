import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import creditService from "@/services/creditcard"
import type Customer from '@/types/Customer';
import { useLoadingStore } from './loading';
import { useMessageStore } from './message';
import { useSuccessStore } from './success';
import type Creditcard from '@/types/Creditcard';

export const useCreditStore = defineStore('credit', () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const successStore = useSuccessStore();
  const dialog = ref(false);
  // const page = ref(1);
  // const take = ref(2);
  const credits = ref<Creditcard[]>([]);
  const editedCredit = ref<Creditcard>({ creditname: "",creditnumber: "",creditamount: 0 });


 

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedCredit.value= { creditname: "",creditnumber: "", creditamount: 0 };
    }
  });
  async function getCreditcards() {
    loadingStore.isLoading = true
  try {
    const res = await creditService.getCreditcards({});
    credits.value = res.data;
    console.log(res);
  } catch(e) {
    console.log(e);
    messageStore.showError("ไม่สามารถเรียกข้อมูลได้")
  }
  loadingStore.isLoading = false
  }
  async function saveCredit() {
    loadingStore.isLoading = true
    try {
      const res = await creditService.saveCreditcards(editedCredit.value);
      dialog.value = false;
      await getCreditcards();
      successStore.showsuccess("ชำระเงินสำเร็จ")
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถชำระเงินได้")
    }
    loadingStore.isLoading = false
  }
  // async function deleteCustomer(id: number) {
  //   loadingStore.isLoading = true
  //   try {
  //     const res = await customerService.delCustomers(id);
  //     await getCustomers();
  //     successStore.showsuccess("ลบสำเร็จ")
  //   } catch(e) {
  //     console.log(e);
  //     messageStore.showError("ไม่สามารถลบข้อมูล Customer ได้")
  //   }
  //   loadingStore.isLoading = false
  // }

  function editCredit(credit: Creditcard) {
    editedCredit.value = JSON.parse(JSON.stringify(credit));;
    dialog.value = true;
  }

  return {
    dialog,
    credits,
    getCreditcards,
    editedCredit,
    saveCredit,
    editCredit,
  };
});

