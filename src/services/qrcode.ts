import type Customer from "@/types/Customer";
import http from "./axios"
import type Qrcode from "@/types/Qrcode";
import customer from "./customer";
function getQrcodes(params: any) {
  return http.get("/qrcodes", {params: params});
}
function saveQrcodes(qrcode: Qrcode) {

  return http.post("/qrcodes", qrcode);
}
function updateQrcodes(id:number, qrcode: Qrcode) {
  return http.patch(`"/qrcodes"/${id}`, qrcode);
}

function delQrcodes(id:number) {
  return http.delete(`/qrcodes/${id}`);
}
export default { getQrcodes,saveQrcodes,updateQrcodes, delQrcodes};