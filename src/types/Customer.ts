export default interface Customer{
  id?: number;
  age: number;
  name: string;
  tel: string;
  gender: string;
  points: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}