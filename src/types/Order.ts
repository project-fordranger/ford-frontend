import type Customer from "./Customer";
import type OrderItem from "./OrderItem";

export default interface Order{
  id?: number;
  amount?: number;
  total?: number;
  discount?: number;
  customerId?: Customer; // Customer Id
  createdDate?: string;
  updatedDate?: string;
  deletedDate?: string;
  orderItems?: OrderItem[];
}



