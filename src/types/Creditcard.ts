export default interface Cash {
    id?: number;
    creditname: string;
    creditnumber: string;
    creditamount: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}