import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import employeeService from "@/services/employee"
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Employee from "@/types/Employee";
import { useSuccessStore } from "./success";
export const useEmployeeStore = defineStore("employee", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const successStore = useSuccessStore();
  const dialog = ref(false);
  const employees = ref<Employee[]>([]);
  const editedEmployee = ref<Employee & {files: File[] }>({ name: "", address: "",tel: "",email: "",position: "",hourly: 0,timetowork: "",timeoff: "",salary: 0,image:"No-Image-Placeholder.jpg",files: []});


  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedEmployee.value= { name: "", address: "",tel: "",email: "",position: "",hourly: 0,timetowork: "",timeoff: "",salary: 0,image:"No-Image-Placeholder.jpg",files: []};
    }
  });
  async function getEmployees() {
    loadingStore.isLoading = true
  try {
    const res = await employeeService.getEmployees();
    employees.value = res.data;
    console.log(res);
  } catch(e) {
    console.log(e);
    messageStore.showError("ไม่สามารถเรียกข้อมูล Employee ได้")
  }
  loadingStore.isLoading = false
  }
  async function saveEmployee() {
    loadingStore.isLoading = true
    try {
      if(editedEmployee.value.id) {
        const res = await employeeService.updateEmployees(editedEmployee.value.id,editedEmployee.value);
      } else {
        const res = await employeeService.saveEmployees(editedEmployee.value);
      }
      dialog.value = false;
      await getEmployees();
      successStore.showsuccess("บันทึกสำเร็จ")
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Employee ได้")
    }
    loadingStore.isLoading = false
  }
  async function deleteEmployee(id: number) {
    loadingStore.isLoading = true
    try {
      const res = await employeeService.delEmployees(id);
      await getEmployees();
      successStore.showsuccess("ลบสำเร็จ")
    } catch(e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Employee ได้")
    }
    loadingStore.isLoading = false
  }

  function editEmployee(employee: Employee) {
    editedEmployee.value = JSON.parse(JSON.stringify(employee));;
    dialog.value = true; 
  }

  return {
    dialog,
    employees,
    getEmployees,
    editedEmployee,
    saveEmployee,
    deleteEmployee,
    editEmployee

  };
});
